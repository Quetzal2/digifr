# DigiFR's origins

The DigiFR project is the result of the effort to **adapt and optimize** the ADNW keyboard layout (a neo variant) for the **french language**, creating an very efficient way of writing and keeping the **power of the neo keyboard layout** design.
